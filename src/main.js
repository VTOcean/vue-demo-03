import Vue from 'vue'
import App from './App.vue'

import VueCal from 'vue-cal';
import 'vue-cal/dist/vuecal.css'

import DatePicker from 'vue2-datepicker';
import 'vue2-datepicker/index.css';

import Treeselect from '@riophae/vue-treeselect'
import '@riophae/vue-treeselect/dist/vue-treeselect.css'

import Multiselect from 'vue-multiselect'
import "vue-multiselect/dist/vue-multiselect.min.css"

import KProgress from 'k-progress';
import Paginate from 'vuejs-paginate'
import JsonExcel from "vue-json-excel";

Vue.component('vue-cal', VueCal);
Vue.component('date-picker', DatePicker);
Vue.component('treeselect', Treeselect);
Vue.component('multiselect', Multiselect);
Vue.component('k-progress', KProgress);
Vue.component('paginate', Paginate)
Vue.component("downloadExcel", JsonExcel);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
